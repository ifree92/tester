#Tester - система тестирования 

## Установка и конфигурироване

Для установки системы тестирования, скопируйте все файлы в папку на сервере и установите базу данных.

Переименуйте файл "config.default.php" в "config.php". Откройте файл и измените значение в зависимости от ваших настроек

Замените **db_host** на место размещения БД (обычно localhost)

Замените **db_user_name** на имя пользователя БД

Замените **db_password** на пароль пользователя БД

Замените **db_name** на имя базы данных

Замените **link_to_api** на ссылку к php файлу API (напр. http://localhost:8888/tester/teste/api.php)

Переименуйте файл "config.default.js" в "config.js". Откройте файл и измените значение в зависимости от ваших настроек

Замените **link_to_api** на ссылку к php файлу API (напр. http://localhost:8888/tester/teste/api.php)

## API

###Получение списка всех групп
api.php?action=get_groups

###Получение списка всех вариантов теста	----	UPDATE!
api.php?action=get_variants&is_lock={!~}

###Вызов функции добавляет запись в БД о начале сессии.
api.php?action=start_session&name={~}&id_group={~}&id_variant={~}

###Получение списка вопросов и вариантов ответа
api.php?action=get_questions&id_session={~}

###Добавление записи в БД об ответе на тестовый вопрос
api.php?action=add_answer&id_session={~}&id_answer={~}

###Заблокировать вариант
api.php?action=lock_variant&id_variant={~}

###Разблокировать все варианты
api.php?action=unlock_all_variants

###Разблокировать вариант
api.php?action=unlock_variant&id_variant={~}

###Залогиниться (проверка логина/пароля, возврат id в случае успеха)
api.php?action=log_in&login={~}&pass={~}

###Получить статистику результатов
api.php?action=get_statistic&id_group={!~}

###Добавить новую группу
api.php?action=new_group&id_group={~}

###Редактировать группу
api.php?action=update_group&id_group={~}&group_name={~}

###Удалить группу
api.php?action=delete_group&id_group={~}

###Редактировать вариант
api.php?action=edit_variant&id_variant={~}&variant_name={~}&is_lock={~}

###Добавить новый вариант
api.php?action=new_variant&variant_name={~}&is_lock={~}

###Удалить вариант
api.php?action=delete_variant&id_variant={~}

###Добавить новый вопрос
api.php?action=new_question&id_variant_FK={~}&text={~}

###Добавить новый вариант ответа
api.php?action=new_answer&id_question_FK={~}&text={~}&is_true={~}

###Получить все вопросы по заданному варианту
api.php?action=get_questions_by_variant&id_variant={~}

###Получить список вариантов ответов по заданному вопросу
api.php?action=get_answers_by_question&id_question={~}

###Редактировать вопрос
api.php?action=update_question&id_question={~}&text={~}&id_variant={!~}

###Удалить вопрос
api.php?action=delete_question&id_question={~}

###Редактировать вариант ответа
api.php?action=update_answer&id_answer={~}&id_question_FK={!~}&text={!~}&is_true={!~}

###Удалить вариант ответа
api.php?action=delete_answer&id_answer={~}

###Получение списка вариантов ответа заданной сессии по вопросам варианта сессии
api.php?action=get_result&id_session={~}

###Получение результатов тестирования и завершение текущей сессии 
api.php?action=finish_session&id_session={~}

#### {~} - обязательный параметр
#### {!~} - необязательный параметр