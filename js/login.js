function dispatchLogin() {
	var login = document.getElementById("username").value;
	var passw = document.getElementById("password").value;
	if (!login || !passw) return;
	$(".alert-info").html("Подождите...");
	$.ajax({
		type: 'POST',
		url: "/php/api.php?action=log_in&login="+login+"&pass="+passw,
		success: function(data) {
            if (data.length!=0) {
                $.cookie('access',data);
                successLogin();
            } else {
                failLogin();
            }
		},
        error: function(jqXHR) {
            //console.log(jqXHR.status);
            failLogin();
        }
	});
};

function successLogin() {
	$(".alert-info").removeClass("failed");
	$(".alert-info").html("Авторизация успешна");
	window.location = "/admin.html";
};

function failLogin() {
	$(".alert-info").addClass("failed");
	$(".alert-info").html("Ошибка авторизации");
}