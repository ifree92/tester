$(document).ready(startUpFunction);

var idSession = null;
var timeDuration = null;
var timeStart = null;
var testerTimer = null;
var countCurTestAnswer = 0;
var countTestAnswers = null;

function startTimer() {
    var timerTime = new Date();
    var startTimerTime = timeDuration;
    function second() {

        timerTime.setTime(--startTimerTime*1000);
        var timerHours = timerTime.getUTCHours();
        var timerMinutes = (timerTime.getUTCMinutes() < 10 ? '0' : '') + timerTime.getUTCMinutes();
        var timerSeconds = (timerTime.getUTCSeconds() < 10 ? '0' : '') + timerTime.getUTCSeconds();

        $("#navbar-timeleft").html(timerHours + ":" + timerMinutes + ":" + timerSeconds);
        if (startTimerTime == 0) {
            clearInterval(testerTimer);
            outOfTime();
        }
    }
    testerTimer = setInterval(second,1000);
	
}

function outOfTime() {
    tryFinishSession();
}

function requestProcessor(data,callback) {
    $.ajax({
        type: 'POST',
        url: "php/api.php",
        data: data,
        timeout: 10000,
        success: callback,
        error: function() {
            console.log("request: " + data + "... error!");
            $("#errorTopContainer").html('<div class="alert alert-error" id="msgTopError" style="display:none"><b class="offset1">Превышено время ожидания ответа. Проверьте соединение с сервером и перезагрузите страницу!</b><button type="button" class="close" data-dismiss="alert">&times;</button></div>');
            $("#msgTopError").fadeIn();
        }
    });
}

function requestProcessorErrCall(data,callback,errorback) {
    $.ajax({
        type: 'POST',
        url: "php/api.php",
        data: data,
        timeout: 10000,
        success: callback,
        error: errorback
    });
}

function startUpFunction() {
    $("#btn-accept-variant").hide();
    $("#select-variant-capture").hide();
    $("#variant-selector").hide();
    $("#client-lockvar-indicator").hide();
    $(".start-test").hide();
    $("#student_name").val("");
    $("#loading-view").fadeIn();
    $("#navbar-timeleft").html("0:00:00");
    requestProcessor("action=get_groups",drawGroupView);
}

function drawGroupView(data) {
    $("#loading-view").hide();
    var jsonGroups = JSON.parse(data);
    $("#group-selector").each(function(){
        $(this).html("");
        $(this).append('<option selected disabled>Перелік груп</option>');
        for (var i in jsonGroups) {
            $(this).append('<option value="' + jsonGroups[i].id_group + '">' + jsonGroups[i].name_group + '</option>');
        }
    });
    $("#input-container").fadeIn();
}

$("#group-selector").change(function(){
    $("#client-group-indicator").show();
    $("#navbar-group").html($("#group-selector option:selected").text());
    requestProcessor("action=get_variants&is_lock=0",drawVariantView);
});

var selectVariant = false;

function drawVariantView(data) {
    $("#client-group-indicator").hide();
    var jsonVariants = JSON.parse(data);
    $("#variant-selector").removeProp("disabled");
    $("#variant-selector").each(function(){
        $(this).html("");
        $(this).append('<option selected disabled>Перелік варіантів</option>');
        for (var i in jsonVariants) {
            $(this).append('<option value="' + jsonVariants[i].id_variant + '">' + jsonVariants[i].name_variant + '</option>');
        }
    });
    $(".select-variant").show();
    $("#btn-accept-variant").each(function(){
        $(this).css("display","inline-block");
        $(this).addClass("disabled");
    });

}

$("#variant-selector").change(function(){
    $("#navbar-variant").html($("#variant-selector option:selected").text());
    $("#btn-accept-variant").removeClass("disabled");
    selectVariant = true;

});

function acceptSelectLockVariant() {
    if (selectVariant) {
        $("#btn-accept-variant").addClass("disabled");
        $("#variant-selector").prop("disabled","true");
        $("#client-lockvar-indicator").fadeIn();
        requestProcessor("action=lock_variant&id_variant="+$("#variant-selector option:selected").val(),successLockVariant);
    }
}

function successLockVariant(data) {
    var val = JSON.parse(data);

    if (val.success == 0) {
        // error lock variant
        $("#errorTopContainer").html('<div class="alert alert-error" id="msgTopError" style="display:none"><b class="offset2">Ошибка выбора варианта! Вариант заблокирован, выберите другй вариант.</b><button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        $("#msgTopError").fadeIn();
        // hide and back all
        $("#select-variant-capture").hide();
        $("#variant-selector").each(function(){
            $(this).removeProp("disabled");
            $(this).hide();
        });
        $("#btn-accept-variant").hide();
        $("#client-lockvar-indicator").hide();
        $("#client-group-indicator").fadeIn();

        // reload variants
        requestProcessor("action=get_variants&is_lock=0",drawVariantView);
    } else {
        $("#client-lockvar-indicator").hide();
        $("#errorTopContainer").html("");
        selectVariant = false;
        $(".start-test").fadeIn();
    }

}

$(".start-test").click(startSession);
$("#student_name").each(function(){

    $(this).keyup(function(){
        $("#navbar-student").html($(this).val());
        if (! $(this).val() ) {
            $(".control-group").addClass("error");
            $(".error-text").fadeTo("fast",1);
        }
        else {
            $(".control-group").removeClass("error");
            $(".error-text").fadeTo("fast",0);
        }

    });
});

function startSession() {
    var studentName = $("#student_name").val();
    var idGroup = $("#group-selector").val();
    var idVariant = $("#variant-selector").val();

    if (studentName && idGroup && idVariant ) {
        $("#input-container").hide();
        $("#loading-view").fadeIn();
        requestProcessor("action=start_session&name=" + studentName + "&id_group=" + idGroup + "&id_variant=" + idVariant,getQuestionsStartedSession);
    } else if (!studentName) {
        $(".control-group").addClass("error");
        $(".error-text").fadeTo("fast",1);
    }
}

function getQuestionsStartedSession(data) {
    var jsonStartedSession = JSON.parse(data);
    idSession = jsonStartedSession.id_session;
    timeDuration = jsonStartedSession.time_duration;
    timeStart = jsonStartedSession.time_start;
    startTimer();
    requestProcessor("action=get_questions&id_session=" + idSession,drawAllTests);
}

function drawAllTests(data) {
    $("#loading-view").hide();
    $("#list-question").html("");
    $("#content-question").html("");
    drawingTestsFromJson(JSON.parse(data));
    $("#question-container").fadeIn();
}

function drawingTestsFromJson(quest) {
    countTestAnswers = quest.length;
    if (countTestAnswers > 0) {
        for (var i in quest) {
            $("#list-question").append('<li><i id="' + quest[i].id_question + '-icon" class="icon-ok-circle"></i><a href="#' + quest[i].id_question + '-q" data-toggle="tab">Питання ' + (parseInt(i)+1) + '</a></li>');
            var tabContentView = '<div class="well tab-pane" id="' + quest[i].id_question + '-q"><div class="row"><div class="span10 question-content">';
            tabContentView += '<h4>' + quest[i].text_question + '</h4>';
            if (quest[i].count_rights == 1) {
                for (var j in quest[i].answer) {
                    tabContentView += '<label class="radio"><input type="radio" name="' + quest[i].id_question + '-question" value="' + quest[i].answer[j].id_answer + '">';
                    tabContentView += quest[i].answer[j].text_answer + '</label><br>';
                }
            } else if (quest[i].count_rights > 1) {
                for (var j in quest[i].answer) {
                    tabContentView += '<label class="checkbox"><input type="checkbox" name="' + quest[i].id_question + '-question" value="' + quest[i].answer[j].id_answer + '">';
                    tabContentView += quest[i].answer[j].text_answer + '</label>';
                }
            }
            tabContentView += '</div></div><br>';
            tabContentView += '<img class="request-indicator-question" src="img/spinner-mini.gif">';
            tabContentView += '<div class="row question-btn"><div class="span1 offset0">';
            tabContentView += '<a class="btn btn-success" onclick="successClick(event)" id-question="' + quest[i].id_question + '" id="' + quest[i].id_question + '-btn-success" href="#">Відповісти</a></div><div class="span1 offset7">';
            tabContentView += '<a class="btn btn-danger pull-left btn-finish-test" onclick="finishTestClick()" href="#" >Завершити</a></div>';
            tabContentView += '</div></div>';
            $("#content-question").append(tabContentView);
        }
        $("#list-question li:first-child").addClass("active");
        $("#content-question div:first-child").addClass("active");
    } else {
        $("#errorTopContainer").html('<div class="alert alert-error" id="msgTopError" style="display:none;"><b class="offset2">Вариант пуст! Вопросы и ответы не заданы для данного варианта.</b><br><a class="btn btn-danger offset4" onclick="resetAll()" href="#"><i class="icon-refresh"></i> Попробовать снова</a><button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        $("#msgTopError").fadeIn();
        clearInterval(testerTimer);
    }
}

var processRequestAnswer = false;
var checkDivId;
function successClick(e) {
    if (processRequestAnswer == false) {
        checkDivId = e.target.attributes["id-question"].value;
        var result = $("#" + checkDivId + "-q input:checked");
        if (((e.target.className.indexOf("disabled") + 1) == 0) && result.length > 0 ) {
            processRequestAnswer = true;
            $("#" + checkDivId + "-q input").prop("disabled", "true");
            $("#" + checkDivId + "-btn-success").addClass("disabled");
            $(".btn-finish-test").addClass("disabled");
            $(".request-indicator-question").show();
            var answers = "";
            for (var i = 0; i < result.length; ++i) {
                answers += result[i].value + ",";
            }
            if (answers) {
                answers = answers.substr(0,answers.length - 1);
                requestProcessorErrCall("action=add_answer&id_session=" + idSession + "&id_answer=" + answers,successRequestAnswer,errorRequestAnswer);
            }
        }
    }
}
var reTryAnswer = false;
function errorRequestAnswer(data) {
    $("#errorTopContainer").html('<div class="alert alert-error" id="msgTopError" style="display:none"><b class="offset1">Отсутствует соединение с сервером! Проверьте соединение с сервером и попробуйте снова!</b><button type="button" class="close" data-dismiss="alert">&times;</button></div>');
    $("#msgTopError").fadeIn();
    $(".btn-finish-test").removeClass("disabled");
    $("#" + checkDivId + "-q input").removeAttr("disabled");
    $("#" + checkDivId + "-btn-success").removeClass("disabled");
    $(".request-indicator-question").hide();
    processRequestAnswer = false;
    reTryAnswer = true;
}

function successRequestAnswer() {
    if (reTryAnswer) $("#errorTopContainer").html("");
    $(".btn-finish-test").removeClass("disabled");
    $("#" + checkDivId + "-icon").fadeIn();
    $(".request-indicator-question").hide();
    processRequestAnswer = false;
    countCurTestAnswer++;
    if (countCurTestAnswer == countTestAnswers) {
        tryFinishSession();
    }
}

function finishTestClick() {
    if (countCurTestAnswer != countTestAnswers) {
        $("#finish_test_warning").modal("show");
    }
}

var reTryFinishSession = false;

function tryFinishSession() {
    if (!reTryFinishSession) {
        $("#question-container").hide();
        $("#loading-view").fadeIn();
    } else {
        $("#errorTopContainer").html("");
    }
    requestProcessorErrCall("action=finish_session&id_session=" + idSession,tryGetResults,errorFinishSession);
}

function errorFinishSession() {
    $("#errorTopContainer").html('<div class="alert alert-error" id="msgTopError" style="display:none;"><b class="offset1">Ошибка завершения сессии! Возможно нет соединения с сервером, попробуйте снова.</b><br><a class="btn btn-danger offset4" onclick="tryFinishSession()" href="#"><i class="icon-refresh"></i> Попробовать снова</a><button type="button" class="close" data-dismiss="alert">&times;</button></div>');
    $("#msgTopError").fadeIn();
    reTryFinishSession = true;
}

var reDrawResult = false;

function tryGetResults(data) {

    if (!reDrawResult) {
        clearInterval(testerTimer);
        var finishData = JSON.parse(data);
        var bodyTableStatistic = '<tr>';
        $("#navbar-timeleft").html("0:00:00");
        console.log(finishData);
        var finishDate = new Date(finishData[0].time_finish * 1000);
        var durationTime = new Date((finishData[0].time_finish - finishData[0].time) * 1000);

        var stringFinishDate = finishDate.getDate() > 9 ? finishDate.getDate() : "0" + finishDate.getDate();
        stringFinishDate += ".";
        stringFinishDate += finishDate.getMonth() > 9 ? finishDate.getMonth() : "0" + finishDate.getMonth();
        stringFinishDate += ".";
        stringFinishDate += finishDate.getFullYear();

        var stringDurationTime = durationTime.getUTCHours();
        stringDurationTime += ":";
        stringDurationTime += durationTime.getUTCMinutes() > 9 ? durationTime.getUTCMinutes() : "0" + durationTime.getUTCMinutes();
        stringDurationTime += ":";
        stringDurationTime += durationTime.getUTCSeconds() > 9 ? durationTime.getUTCSeconds() : "0" + durationTime.getUTCSeconds();

        bodyTableStatistic += '<td>' + stringFinishDate + '</td>'; // Дата сдачи
        bodyTableStatistic += '<td>' + stringDurationTime + '</td>'; // Продолжительность теста
        bodyTableStatistic += '<td>' + finishData[0].right_questions + '/' + finishData[0].count_questions + '</td>'; // Правильные ответы
        bodyTableStatistic += '<td>' + ((finishData[0].right_questions / finishData[0].count_questions) * 100).toFixed(2) + '%</td>'; // Оценка
        bodyTableStatistic += '</tr>';
        $("#put-tbody-statistic").html(bodyTableStatistic);
        $("#loading-view").hide();
        $("#put-tbody-result").html("");
        $("#btn-all-reset").hide();
        $("#container-resultat").fadeIn();
        $("#loading-result-table").fadeIn();
    } else $("#errorTopContainer").html("");
    requestProcessorErrCall("action=get_result&id_session=" + idSession,drawResult,errorDrawResult);
}

function errorDrawResult() {
    $("#errorTopContainer").html('<div class="alert alert-error" id="msgTopError" style="display:none;"><b class="offset1">Ошибка получения результатов! Возможно нет соединения с сервером, попробуйте снова.</b><br><a class="btn btn-danger offset4" onclick="tryGetResults()" href="#"><i class="icon-refresh"></i> Попробовать снова</a><button type="button" class="close" data-dismiss="alert">&times;</button></div>');
    $("#msgTopError").fadeIn();
    reDrawResult = true;
}

function drawResult(data) {
    var testResult = JSON.parse(data);
    var strResultTableHTML = "";
    for (var i in testResult) {
        strResultTableHTML += '<tr>';
        if (testResult[i].answers.length == 0) {
            strResultTableHTML += '<td class="error-td">' + testResult[i].text + '</td><td class="error-td">---</td></tr>';
        }
        else if (testResult[i].answers.length > 0) {
            strResultTableHTML += '<td class="' + (testResult[i].is_right_question==1 ? "success-td" : "error-td") + '" rowspan="' + testResult[i].answers.length + '">' + testResult[i].text + '</td>';
            strResultTableHTML += '<td class="' + (testResult[i].answers[0].is_true==1 ? "success-td" : "error-td") + '">' + testResult[i].answers[0].text + '</td></tr>';
            for (var j=1; j<testResult[i].answers.length; ++j) {
                strResultTableHTML += '<tr><td class="' + (testResult[i].answers[j].is_true==1 ? "success-td" : "error-td") + '">' + testResult[i].answers[j].text + '</td></tr>';
            }
        }
    }
    $("#loading-result-table").hide();
    $("#put-tbody-result").html(strResultTableHTML);
    $("#btn-all-reset").fadeIn();
}

function resetAll() {
    $("#container-resultat").hide();
    idSession = null;
    timeDuration = null;
    timeStart = null;
    testerTimer = null;
    countCurTestAnswer = 0;
    countTestAnswers = null;
    reDrawResult = false;
    reTryFinishSession = false;
    reTryAnswer = false;
    processRequestAnswer = false;
    selectVariant = false;
    $("#navbar-group").html("");
    $("#navbar-variant").html("");
    $("#navbar-student").html("");
    $("#errorTopContainer").html("");
    startUpFunction();
}