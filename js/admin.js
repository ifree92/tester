var gr_id = null;
var var_id = null;
var quest_id = null;
var answ_id = null;
var ses_id = null;

$(document).ready(function(){
	//datatable
	$('.datatable').dataTable({
			"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span12'i><'span12 center'p>>",
			"sPaginationType": "bootstrap",
			"oLanguage": {
			    "sLengthMenu": "_MENU_ записей на страницу"
			}
		} );

	$("#results_link").click(showResultsContainer);
	$("#tasks_link").click(showTasksContainer);
	$("#groups_link").click(showGroupsContainer);
	$("#config_link").click(showConfigContainer);
	$("#group").change(getResult);
    $("#date").change(getResult);
    $("#btn_add_group").click(addGroup);
    $("#gr_save").click(updateGroup);
    $("#gr_delete").click(deleteGroup);
    $("#btn_add_variant").click(addVariant);
    $("#btn_delete_variant").click(deleteVariant);
    $("#btn_update_variant").click(updateVariant);
    $("#btn_add_question").click(function(){quest_id = null;editQuestion();});
    $("#btn_add_answer").click(function(){answ_id = null;editAnswer();});
    $("#btn_quest_save").click(updateQuestion);
    $("#btn_quest_delete").click(deleteQuestion);
    $("#btn_answer_save").click(updateAnsver);
    $("#btn_answer_delete").click(deleteAnswer);
    $("#btn_unlock_all_variant").click(function(){var_id=null;unlockVariant();});
    $("#btn_unlock_variant").click(function(){if (var_id) unlockVariant();});
    $("#btn_pass_change").click(changePass);
    $("#btn_download_variant").click(downloadVariant);
    $("#btn_upload_variant").click(uploadVariant);
    $("#btn_clear_variant").click(clearVariant);
    $("#btn_set_time").click(setTime);

    $("#variant_table").dataTable({"bPaginate":false,"bFilter":false,"bSortClasses":false,"bSort":false});
    $("#results_popup_table").dataTable({"bPaginate":false,"bFilter":false,"bSortClasses":false,"bSort":false,"bInfo":false});
    $("#group_table").dataTable({"bPaginate":false,"bFilter":false});
    $("#question_table").dataTable({"bPaginate":false,"bFilter":false});
    $("#answer_table").dataTable({"bPaginate":false,"bFilter":false,"bInfo":false,"bSortClasses":false});

    showResultsContainer();
});

function setTime() {
    var time = $("#date01")[0].valueAsNumber;
    time/=1000;
        $("#btn_set_time").prop("disabled",true);
        var seconds = $()
        $.ajax({
            type: 'POST',
            url: "/php/api.php",
            data: "action=time_duration_set&seconds="+time,
            success: function(data) {
                console.log(data);
                $("#btn_set_time").prop("disabled",false);
            },
            error: function() {
                console.log("error");
                $("#btn_set_time").prop("disabled",false);
            }
        });
}

function getTime() {
    $.ajax({
        type: 'POST',
        url: "/php/api.php",
        data: "action=time_duration_get",
        success: function(da) {
            var sec = parseInt(da);
//            var h=0;
//            var m=0;
//            if (sec) {
//                m=Math.floor(sec/60);
//                h=Math.floor(m/60);
//            }
            //$("#date01").val(((h<10)?"0"+h:h)+":"+((m<10)?"0"+m:m));
            $("#date01")[0].valueAsNumber = sec*1000;
        },
        error: function() {

        }
    });
}

function clearVariant() {
    $("#btn_clear_variant").prop("disabled",true);
    $.ajax({
        type: 'POST',
        url: "/php/api.php",
        data: "action=clear_db_var_dump",
        success: function(data) {
            $("#btn_clear_variant").prop("disabled",false);
            $("#upload_variant_popup").modal("hide");
            var_id = null;
            showVariants();
        },
        error: function() {
            $("#btn_clear_variant").prop("disabled",false);
        }
    });
}

function uploadVariant() {
    $("#upload_variant_popup").modal("show");
}

function downloadVariant() {
    window.open("/php/api.php?action=get_db_var_dump",'_blank');
}

function changePass() {
    var newpass = $("#newpass").val();
    var newpass2 = $("#newpass2").val();
    if (newpass=="") return;
    if (newpass === newpass2) {
        $("#btn_pass_change").prop("disabled",true);
        $.ajax({
            type: 'POST',
            url: "/php/api.php",
            data: "action=change_password&pass="+newpass,
            success: function(data) {
                $.cookie('access',"");
                window.location = "/login.html";
            },
            error: function() {
                $("#btn_pass_change").prop("disabled",false);
                console.log("change pass error");
            }
        });
    } else {
        $("#newpass2").val("");
    }
}

function getGroup(callback) {
	$.ajax({
		type: 'POST',
		url: "/php/api.php",
        data: "action=get_groups",
		success: function(data) {
			try {
				var res = JSON.parse(data);
				if (res.e) {
					console.log(data);
					return;
				}
				callback(res);
			} catch (e) {
				console.log(data);
                callback(null);
			}
		}
	});
}

function getResult() {	//получить статистику результатов
	var filter = "";
	if($("#group")[0].selectedIndex>0) {
		filter += "&id_group="+$("#group").val();
	}
    if ($("#date").val() != "") {
        var date = new Date($("#date").val());
        filter += "&date="+date.getTime()/1000;
    }
	$.ajax({
		type: 'POST',
		url: "/php/api.php",
        data: "action=get_statistic"+filter,
		success: function(data) {
            var table = $("#results_table").dataTable();
            table.fnClearTable();
			try {
				var res = JSON.parse(data);
				for(var i=0;i<res.length;++i) {
					var date = new Date(res[i].time*1000);

                    var elapsedSeconds = Math.floor(parseInt(res[i].time_finish) - parseInt(res[i].time));
                    var elapsedMinutes = 0;
                    if (elapsedSeconds <=0 || isNaN(elapsedSeconds)) {
                        elapsedSeconds = 0;
                    } else {
                        elapsedMinutes = Math.floor(elapsedSeconds/60);
                        elapsedSeconds -= (elapsedMinutes*60);
                    }

                    var rang = 0;
                    try {
                        rang = Math.round((parseInt(res[i].right_questions)/parseInt(res[i].count_questions))*100);
                        if (isFinite(rang)==false) rang = 0;
                    } catch(e) {
                        rang = 0;
                    }
                    var row = table.fnAddData( [
                        res[i].description,
                        res[i].group_name,
                        res[i].variant_name,
                        date.getDate()+"."+(date.getMonth()+1)+"."+date.getFullYear(),
                        date.getHours()+":"+date.getMinutes(),
                        elapsedMinutes+":"+((elapsedSeconds<10)?("0"+elapsedSeconds):elapsedSeconds),
                        res[i].right_questions+"/"+res[i].count_questions,
                        rang+"%" ]
                    );
                    var tr = table.fnGetNodes(row[0]);
                    tr.setAttribute("value",res[i].id_session);
				}
                var table_tr = $("#results_table tbody tr");
                table_tr.die("click");
                table_tr.live("click",function(event){
                    ses_id = this.getAttribute("value");
                    var row = $(this).children();
                    showResultPopup(row[0].innerHTML,row[1].innerHTML,row[2].innerHTML,row[3].innerHTML+" / "+row[4].innerHTML,row[6].innerHTML,row[7].innerHTML);
                });
			} catch (e) {
				console.log(e+data);
			} finally {
                $("#result_indicator").hide();
            }
		},
        error: function() {
            $("#result_indicator").hide();
            $("#results_table").dataTable().fnClearTable();
        }
	});
    $("#result_indicator").show();
}

function showResultPopup(name,group,variant,data,answers,rating) {
    if (ses_id) {
        $("#result_form_name")[0].innerHTML = group+" : "+name;
        $("#result_form_info")[0].innerHTML = "Вариант: "+variant+"<br>Дата: "+data+"<br>Ответы:"+answers+" Оценка:"+rating;
        $("#result_popup").modal("show");

        var table = $("#results_popup_table").dataTable();
        table.fnClearTable();
        $.ajax({
            type: 'POST',
            url: "/php/api.php",
            data: "action=get_result&id_session="+ses_id,
            success: function(data) {
                try {
                    var res = JSON.parse(data);
                    for(var i=0;i<res.length;++i) {
                        var qtext = res[i].text;
                        for(var j=0;j<res[i].answers.length;++j) {
                            var row = table.fnAddData([qtext,res[i].answers[j].text]);
                            var tr = table.fnGetNodes(row[0]);
                            if (res[i].answers[j].is_true == "0") {
                                $(tr).addClass("highlighted");
                            }
                            qtext="";
                        }
                    }
                } catch (e) {
                    console.log(e+data);
                } finally {
                    $("#result_popup_indicator").hide();
                }
            },
            error: function() {
                $("#result_popup_indicator").hide();
            }
        });
        $("#result_popup_indicator").show();
    }
}

function showResultsContainer(){
	$("#tasks_container").fadeOut('fast', function(){
		$("#groups_container").fadeOut('fast', function(){
			$("#config_container").fadeOut('fast', function(){
				$("#results_container").fadeIn('fast');
			});
		});
	});
    $("#group")[0].selectedIndex = 0;
	getResult();
	getGroup(function(res) {
		$("#group").html("<option>Все группы</option>");
		for (var i = 0; i<res.length; ++i) {
			$("#group").append('<option value="' + res[i].id_group + '">' + res[i].name_group + '</option>');	
		}
	});
}

function showTasksContainer(){
	$("#results_container").fadeOut('fast', function(){
		$("#groups_container").fadeOut('fast', function(){
			$("#config_container").fadeOut('fast', function(){
				$("#tasks_container").fadeIn('fast');
			});
		});
	});
    showVariants();
}

function showGroupsContainer(){
	$("#tasks_container").fadeOut('fast', function(){
		$("#results_container").fadeOut('fast', function(){
			$("#config_container").fadeOut('fast', function(){
				$("#groups_container").fadeIn('fast');
			});
		});
	});
    showGroups();
}

function showGroups() {
    getGroup(function(res) {
        if (res) {
            $("#group_table").dataTable().fnClearTable();
            for (var i = 0; i<res.length; ++i) {
                var row = $("#group_table").dataTable().fnAddData([res[i].name_group]);
                var tr = $("#group_table").dataTable().fnGetNodes(row[0]);
                tr.setAttribute("value",res[i].id_group);
            }
            var table_tr = $("#group_table tbody tr");
            table_tr.die("click");
            table_tr.live("click",function(event){
                editGroup(this.getAttribute("value"),this.firstChild.innerHTML);
            });
        }
        $("#group_indicator").hide();
    });
    $("#group_indicator").show();
}

function editGroup(id,name){
    if (id) {
        $("#gr_name").val(name);
        gr_id = id;
        grEditorEnableButtons();
        $('#gr_edit_popup').modal('show');
    }
}

function updateGroup() {
    $.ajax({
        type: 'POST',
        url: "/php/api.php",
        data: "action=update_group&id_group="+gr_id+"&group_name="+$("#gr_name").val(),
        success: function(data) {
            $("#group_editor_indicator").hide();
            $('#gr_edit_popup').modal('hide');
            showGroups();
        },
        error: function() {
            $("#group_editor_indicator").hide();
        }
    });
    grEditorDisbleButtons();
    $("#group_editor_indicator").show();
}

function deleteGroup() {
    $.ajax({
        type: 'POST',
        url: "/php/api.php",
        data: "action=delete_group&id_group="+gr_id,
        success: function(data) {
            $("#group_editor_indicator").hide();
            $('#gr_edit_popup').modal('hide');
            showGroups();
        },
        error: function() {
            $("#group_editor_indicator").hide();
        }
    });
    grEditorDisbleButtons();
    $("#group_editor_indicator").show();
}

function addGroup() {
    if ($("#new_group_name").val() != "") {
        $.ajax({
            type: 'POST',
            url: "/php/api.php",
            data: "action=new_group&group_name="+$("#new_group_name").val(),
            success: function(data) {
                $("#btn_add_group").prop("disabled",false);
                $("#new_group_name").val("");
                showGroups();
            },
            error: function() {
                $("#group_indicator").hide();
                $("#btn_add_group").prop("disabled",false);
            }
        });
        $("#btn_add_group").prop("disabled",true);
        $("#group_indicator").show();
    }
}

function grEditorDisbleButtons() {
    $("#gr_delete").prop("disabled",true);
    $("#gr_save").prop("disabled",true);
}
function grEditorEnableButtons() {
    $("#gr_delete").prop("disabled",false);
    $("#gr_save").prop("disabled",false);
}

function showVariants() {
    $.ajax({
        type: 'POST',
        url: "/php/api.php?action=get_variants",
        success: function(data) {
            try {
                var var_table = $("#variant_table").dataTable();
                var_table.fnClearTable();
                var res = JSON.parse(data);

                for (var i = 0; i<res.length; ++i) {
                    var row = var_table.fnAddData([res[i].name_variant]);
                    var tr = var_table.fnGetNodes(row[0]);
                    tr.setAttribute("value",res[i].id_variant);
                    if (res[i].is_lock == "1") $(tr).addClass("highlighted");
                }
                var table_tr = $("#variant_table tbody tr");
                table_tr.die("click");
                table_tr.live("click",function(event){
                    var_id = this.getAttribute("value");
                    showQuestions(this.getAttribute("value"),this.firstChild.innerHTML);
                });
            } catch (e) {
                console.log(data);
            } finally {
                $("#variant_indicator").hide();
            }
        },
        error: function() {
            $("#variant_table").dataTable().fnClearTable();
            $("#variant_indicator").hide();
            showQuestions();
        }
    });
    $("#variant_indicator").show();
}

function addVariant() {
    var newName = $("#new_variant_name").val();
    if (newName != "") {
        $.ajax({
            type: 'POST',
            url: "/php/api.php",
            data: "action=new_variant&variant_name="+newName+"&is_lock=0",
            success: function(data) {
                $("#btn_add_variant").prop("disabled",false);
                $("#new_variant_name").val("");
                showVariants();
            },
            error: function() {
                $("#variant_indicator").hide();
                $("#btn_add_variant").prop("disabled",false);
            }
        });
        $("#btn_add_variant").prop("disabled",true);
        $("#variant_indicator").show();
    }
}

function updateVariant() {
    if (var_id) {
        $.ajax({
            type: 'POST',
            url: "/php/api.php",
            data: "action=edit_variant&id_variant="+var_id+"&variant_name="+$("#variant_name").val()+"&is_lock=0",
            success: function(data) {
                questionButtonsDisable(false);
                $("#question_indicator").hide();
                showVariants();
            },
            error: function() {
                questionButtonsDisable(false);
                $("#question_indicator").hide();
            }
        });
        questionButtonsDisable(true);
        $("#question_indicator").show();
    }
}

function unlockVariant() {
    if (!var_id) {
        $.ajax({
            type: 'POST',
            url: "/php/api.php",
            data: "action=unlock_all_variants",
            success: function(data) {
                $("#variant_indicator").hide();
                showVariants();
            },
            error: function() {
                $("#variant_indicator").hide();
            }
        });
        $("#variant_indicator").show();
    } else {
        $.ajax({
            type: 'POST',
            url: "/php/api.php",
            data: "action=unlock_variant&id_variant="+var_id,
            success: function(data) {
                $("#question_indicator").hide();
                showVariants();
            },
            error: function() {
                $("#question_indicator").hide();
            }
        });
        $("#question_indicator").show();
    }
}

function deleteVariant() {
    if (var_id) {
        $.ajax({
            type: 'POST',
            url: "/php/api.php",
            data: "action=delete_variant&id_variant="+var_id,
            success: function(data) {
                questionButtonsDisable(true);
                quest_id = var_id = null;
                $("#variant_name").val("");
                $("#question_table").dataTable().fnClearTable();
                $("#question_indicator").hide();
                showVariants();
            },
            error: function() {
                questionButtonsDisable(false);
                $("#question_indicator").hide();
            }
        });
        questionButtonsDisable(true);
        $("#question_indicator").show();
    }
}

function questionButtonsDisable(val) {
    $("#btn_update_variant").prop("disabled",val);
    $("#btn_delete_variant").prop("disabled",val);
    $("#btn_add_question").prop("disabled",val);
    $("#btn_unlock_variant").prop("disabled",val);
}

function showQuestions(variantId,variantName) {
    if (variantId) {
        var_id = variantId;
    } else {
        if (!var_id) {
            var_id = false;
            $("#variant_name").val("");
            $("#question_table").dataTable().fnClearTable();
            questionButtonsDisable(true);
        }
    }
    if (var_id) {
        $("#variant_name").val(variantName);
        $("#question_table").dataTable().fnClearTable();
        $.ajax({
            type: 'POST',
            url: "/php/api.php",
            data: "action=get_questions_by_variant&id_variant="+var_id,
            success: function(data) {
                try {
                    var quest_table = $("#question_table").dataTable();
                    quest_table.fnClearTable();
                    var res = JSON.parse(data);

                    for (var i = 0; i<res.length; ++i) {
                        var row = quest_table.fnAddData([res[i].text]);
                        var tr = quest_table.fnGetNodes(row[0]);
                        tr.setAttribute("value",res[i].id_question);
                    }
                    var table_tr = $("#question_table tbody tr");
                    table_tr.die("click");
                    table_tr.live("click",function(event){
                        quest_id = this.getAttribute("value");
                        editQuestion(this.firstChild.innerHTML);
                    });
                } catch (e) {
                    console.log(data);
                } finally {
                    questionButtonsDisable(false);
                    $("#question_indicator").hide();
                }
            },
            error: function() {
                questionButtonsDisable(true);
                $("#question_indicator").hide();
                $("#question_table").dataTable().fnClearTable();
            }
        });
        $("#question_indicator").show();
    }
}

function editQuestion(text) {
    if (quest_id) {
        if (text) $("#quest_text").val(text);
        $("#quest_edit_popup").modal("show");
        $("#btn_add_answer").prop("disabled",false);
        showAnswers();
        return;
    }
    if (var_id) {
        $("#quest_text").val("");
        $("#answer_table").dataTable().fnClearTable();
        $("#btn_add_answer").prop("disabled",true);
        $("#quest_edit_popup").modal("show");
    }
}

function updateQuestion() {
    if (var_id) {
        var text = $("#quest_text").val();
        if (text=="") return;
        if (quest_id) { //обновить вопрос
            $.ajax({
                type: 'POST',
                url: "/php/api.php",
                data: "action=update_question&id_question="+quest_id+"&text="+text,
                success: function(data) {
                    $("#question_editor_indicator").hide();
                    showQuestions();
                },
                error: function() {
                    $("#question_editor_indicator").hide();
                }
            });
        } else {    //создать новый вопрос
            $.ajax({
                type: 'POST',
                url: "/php/api.php",
                data: "action=new_question&id_variant_FK="+var_id+"&text="+text,
                success: function(data) {
                    try {
                        var res = JSON.parse(data);
                        if (res.id_question) quest_id = res.id_question;  //сохранить новый id
                        $("#btn_add_answer").prop("disabled",false); //разблокировать кнопку нового ответа
                    } catch (e) {
                        console.log(data);
                    }
                    $("#question_editor_indicator").hide();
                    showQuestions();
                },
                error: function() {
                    $("#question_editor_indicator").hide();
                }
            });
        }
        $("#question_editor_indicator").show();
    }
}

function deleteQuestion() {
    if (quest_id) {
        $.ajax({
            type: 'POST',
            url: "/php/api.php",
            data: "action=delete_question&id_question="+quest_id,
            success: function(data) {
                quest_id = null;
                $("#question_editor_indicator").hide();
                $("#quest_edit_popup").modal("hide");
                showQuestions();
            },
            error: function() {
                $("#question_editor_indicator").hide();
            }
        });
        $("#question_editor_indicator").show();
    }
}

function showAnswers() {
    if (quest_id) {
        $("#answer_table").dataTable().fnClearTable();
        $.ajax({
            type: 'POST',
            url: "/php/api.php",
            data: "action=get_answers_by_question&id_question="+quest_id,
            success: function(data) {
                try {
                    var answ_table = $("#answer_table").dataTable();
                    answ_table.fnClearTable();
                    var res = JSON.parse(data);

                    for (var i = 0; i<res.length; ++i) {
                        var row = answ_table.fnAddData([res[i].text,res[i].is_true]);
                        var tr = answ_table.fnGetNodes(row[0]);
                        tr.setAttribute("value",res[i].id_answer);
                    }
                    var table_tr = $("#answer_table tbody tr");
                    table_tr.die("click");
                    table_tr.live("click",function(event){
                        answ_id = this.getAttribute("value");
                        editAnswer(this.firstChild.innerHTML,this.lastChild.innerHTML);

                    });
                } catch (e) {
                    console.log(data);
                } finally {
                    $("#question_editor_indicator").hide();
                }
            },
            error: function() {
                $("#question_editor_indicator").hide();
            }
        });
        $("#question_editor_indicator").show();
    }
}

function editAnswer(text,istrue) {
    if (answ_id) {
        if (text) $("#answer_text").val(text);
        if (istrue == "1") {
            $("#answer_istrue").prop("checked",true);
            $("#answer_istrue").parent().addClass("checked");
        } else {
            $("#answer_istrue").prop("checked",false);
            $("#answer_istrue").parent().removeClass("checked");
        }
        $("#answer_edit_popup").modal("show");
        return;
    }
    if (quest_id) {
        $("#answer_text").val("");
        $("#answer_istrue").prop("checked",false);
        $("#answer_istrue").parent().removeClass("checked");
        $("#answer_edit_popup").modal("show");
    }
}

function updateAnsver() {
    var text = $("#answer_text").val();
    var istrue = "0";
    if ($("#answer_istrue").prop("checked")) istrue = "1";
    if (answ_id) {
        //console.log("/php/api.php?action=update_answer&id_answer="+answ_id+"&text="+text+"&is_true="+istrue);
        $.ajax({
            type: 'POST',
            url: "/php/api.php",
            data: "action=update_answer&id_answer="+answ_id+"&text="+text+"&is_true="+istrue,
            success: function(data) {
                $("#answer_editor_indicator").hide();
                $("#answer_edit_popup").modal("hide");
                showAnswers();
            },
            error: function() {
                $("#answer_editor_indicator").hide();
            }
        });
    } else {
        $.ajax({
            type: 'POST',
            url: "/php/api.php",
            data: "action=new_answer&id_question_FK="+quest_id+"&text="+text+"&is_true="+istrue,
            success: function(data) {
                $("#answer_editor_indicator").hide();
                $("#answer_edit_popup").modal("hide");
                showAnswers();
            },
            error: function() {
                $("#answer_editor_indicator").hide();
            }
        });
    }
    $("#answer_editor_indicator").show();
}

function deleteAnswer() {
    if (answ_id) {
        $.ajax({
            type: 'POST',
            url: "/php/api.php",
            data: "action=delete_answer&id_answer="+answ_id,
            success: function(data) {
                answ_id = null;
                $("#answer_editor_indicator").hide();
                $("#answer_edit_popup").modal("hide");
                showAnswers();
            },
            error: function() {
                $("#answer_editor_indicator").hide();
            }
        });
        $("#answer_editor_indicator").show();
    } else {
        $("#answer_edit_popup").modal("hide");
    }
}

function showConfigContainer(){
	$("#tasks_container").fadeOut('fast', function(){
		$("#groups_container").fadeOut('fast', function(){
			$("#results_container").fadeOut('fast', function(){
				$("#config_container").fadeIn('fast');
			});
		});
	});
    getTime();
}


