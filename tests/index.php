<?php

require_once ("../config.php");
require_once ("json.php");

header('Content-Type: text/html; charset=utf8');

test();

function test(){

	error_reporting(0);

	printStartMess();

	printTestMess("проверка подключения к MySQL");
	if ( !($mysql_session = mysql_connect(dbHost, dbUser, dbPassword)) ) {
		printError("Cannot connect to DB");
		return;
	}

	printTestMess("проверка подключения к БД");
	if(!mysql_select_db(dbName)){
		printError("DB not found");
		return;
	}

	printTestMess("проверка наличия всех таблиц");
	mysql_query("SET NAMES 'UTF8'");

	$tables = array("admin", "answer", "group", "question", "result", "session", "variant" );

	for($i = 0; $i < count($tables); $i++){
		$table = $tables[$i];
		if(!(mysql_num_rows(mysql_query("SHOW TABLES LIKE '".$table."'"))==1)){
			printError("Table ".$table." not found");
			return;
		}
	}

	printTestMess("проверка доступа к API");
	$html = getHTML(APIServer);

	if(!$html){
		printError("Нету доступа к API");
		return;
	}

	printTestMess("проверка защиты от пустого action");

	$response = getJSONWithParams(array("action" => ""));
	checkIsCorrectErrorMessage($response);

	printTestMess("проверка защиты от неверного action");

	$response = getJSONWithParams(array("action" => "some"));
	checkIsCorrectErrorMessage($response);

	printTestMess("проверка правильности ответа на action=get_groups");

	$response = getJSONWithParams(array("action" => "get_groups"));
	checkIsNotErrorMessage($response);
	checkIsGroupsJSON($response);

	printTestMess("проверка правильности ответа на action=get_variants");

	$response = getJSONWithParams(array("action" => "get_variants"));
	checkIsNotErrorMessage($response);
	checkIsVariantsJSON($response);



	// проверка старта сесии

	printTestMess("проверка защиты от старта сессии с параметрами action=start_session");
	$response = getJSONWithParams(array("action" => "start_session"));
	checkIsCorrectErrorMessage($response);

	printTestMess("проверка защиты от старта сессии с параметрами action=start_session name=some name");
	$response = getJSONWithParams(array("action" => "start_session", "name" => "some name"));
	checkIsCorrectErrorMessage($response);

	printTestMess("проверка защиты от старта сессии с параметрами action=start_session name=some name id_group=1");
	$response = getJSONWithParams(array("action" => "start_session", "name" => "some name", "id_group" =>"1"));
	checkIsCorrectErrorMessage($response);

	printTestMess("проверка защиты от старта сессии с параметрами action=start_session name=some name id_group=1 id_variant=пусто");
	$response = getJSONWithParams(array("action" => "start_session", "name" => "some name", "id_group" =>"1", "id_variant" => ""));
	checkIsCorrectErrorMessage($response);

	printTestMess("проверка старта сессии с неверным id группы");
	$response = getJSONWithParams(array("action" => "start_session", "name" => "", "id_group" =>"10000", "id_variant" => ""));
	checkIsCorrectErrorMessage($response);

	printTestMess("проверка старта сессии с верным id группы но пустым вариантом");
	$response = getJSONWithParams(array("action" => "start_session", "name" => "", "id_group" =>"1", "id_variant" => ""));
	checkIsCorrectErrorMessage($response);

	//утечка структуры таблицы
	printTestMess("проверка старта сессии с верным id группы но неверным вариантом");
	$response = getJSONWithParams(array("action" => "start_session", "name" => "", "id_group" =>"1", "id_variant" => "1000000"));
	checkIsCorrectErrorMessage($response);

	//BUG - проходит запрос
	// printTestMess("проверка старта сессии с верным id группы и верным вариантом но пустым именем");
	// $response = getJSONWithParams(array("action" => "start_session", "name" => "", "id_group" =>"1", "id_variant" => "1"));
	// checkIsCorrectErrorMessage($response);

	//BUG - вывод MySql ошибки
	// printTestMess("проверка старта сессии c ' вместо имени");
	// $response = getJSONWithParams(array("action" => "start_session", "name" => "'", "id_group" =>"1", "id_variant" => "1"));
	// checkIsNotErrorMessage($response);

	printTestMess("проверка старта сессии с верными данными");
	$response = getJSONWithParams(array("action" => "start_session", "name" => "some name", "id_group" =>"1", "id_variant" => "1"));
	checkIsNotErrorMessage($response);
	checkIsValidSessionData($response);


	printTestMess("проверка добавление сессии в таблицу");
	$query = "SELECT * FROM session WHERE id_session='".$response -> id_session."'";
	$queryResult = mysql_query($query);
	if(!$queryResult || (mysql_num_rows($queryResult) == 0)) printError("запись про старт сессии не добавлена в таблицу");

	$testSessionID = $response -> id_session;


	//проверка получения вопросов
	printTestMess("проверка защиты от получения вопросов с пустым id сессии");
	$response = getJSONWithParams(array("action" => "get_questions"));
	checkIsCorrectErrorMessage($response);

	printTestMess("проверка защиты от получения вопросов с неверным параметром");
	$response = getJSONWithParams(array("action" => "get_questions", "id_ses" => "3" ));
	checkIsCorrectErrorMessage($response);

	printTestMess("проверка защиты от получения вопросов с неверным id сессии");
	$response = getJSONWithParams(array("action" => "get_questions", "id_session" => "892734598"));
	checkIsCorrectErrorMessage($response);

	printTestMess("проверка получения вопросов с верным id сессии");
	$response = getJSONWithParams(array("action" => "get_questions", "id_session" => $testSessionID));
	checkIsNotErrorMessage($response);
	checkIsValidQuestionsData($response);

	$testQuestions = $response;


	//проверка добавления ответов
	printTestMess("проверка защиты от добавления ответов с запростом action=add_answer");
	$response = getJSONWithParams(array("action" => "add_answer"));
	checkIsCorrectErrorMessage($response);

	printTestMess("проверка защиты от добавления ответов с запростом action=add_answer id_session=пустое_поле");
	$response = getJSONWithParams(array("action" => "add_answer", "id_session" => ""));
	checkIsCorrectErrorMessage($response);

	printTestMess("проверка защиты от добавления ответов с запростом action=add_answer id_session=существующая сессия");
	$response = getJSONWithParams(array("action" => "add_answer", "id_session" => $testSessionID));
	checkIsCorrectErrorMessage($response);

	printTestMess("проверка защиты от добавления ответов с запростом action=add_answer id_session=существующая_сессия");
	$response = getJSONWithParams(array("action" => "add_answer", "id_session" => $testSessionID));
	checkIsCorrectErrorMessage($response);

	printTestMess("проверка защиты от добавления ответов с запростом action=add_answer id_session=существующая сессия id_answer=пустое_поле");
	$response = getJSONWithParams(array("action" => "add_answer", "id_session" => $testSessionID, "id_answer" => ""));
	checkIsCorrectErrorMessage($response);

	printTestMess("проверка защиты от добавления ответов с запростом action=add_answer id_session=существующая сессия id_answer=левое_значение");
	$response = getJSONWithParams(array("action" => "add_answer", "id_session" => $testSessionID, "id_answer" => "123452345234"));
	checkIsCorrectErrorMessage($response);

	printTestMess("проверка добавление ответа с правильным id ответа");
	$validAnswerId = $testQuestions[0] -> answer[0] -> id_answer;
	$response = getJSONWithParams(array("action" => "add_answer", "id_session" => $testSessionID, "id_answer" => $validAnswerId));
	checkIsNotErrorMessage($response);


	//получение результатов
	printTestMess("проверка защиты от получение результатов с запросом action=get_result");
	$response = getJSONWithParams(array("action" => "get_result"));
	checkIsCorrectErrorMessage($response);

	printTestMess("проверка защиты от получение результатов с запросом action=get_result id_session=пустое_поле");
	$response = getJSONWithParams(array("action" => "get_result", "id_session" => ""));
	checkIsCorrectErrorMessage($response);

	printTestMess("проверка защиты от получение результатов с запросом action=get_result id_session=левое_значение");
	$response = getJSONWithParams(array("action" => "get_result", "id_session" => "234523462645"));
	checkIsCorrectErrorMessage($response);

	printTestMess("проверка защиты от получение результатов с запросом action=get_result id_session=существующая_сессия");
	$response = getJSONWithParams(array("action" => "get_result", "id_session" => $testSessionID));
	checkIsNotErrorMessage($response);


	printFinishMess();
}



//requests

function checkIsValidQuestionsData($response){
	if(!is_array($response)) printError("корневой объект не массив");
	for($i=0; $i<count($response); $i++){
		$question = $response[$i];
		if(!isCorrectStringValue($question -> id_question)) printError("у вопроса не установлен id");
		if(!isCorrectStringValue($question -> text_question)) printError("у вопроса не установлен текст");
		if(!isset($question -> answer)) printError("у вопроса нет вариантов");
		if(!is_array($question -> answer)) printError("варианты ответов на вопрос не сформированы в массив");
		for($j = 0; $j < count($question -> answer); $j++){
			$answer = $question -> answer[$j];
			if(!isCorrectStringValue($answer -> id_answer)) printError("у ответа не установлен id");
			if(!isCorrectStringValue($answer -> text_answer)) printError("у ответа не установлен текст");
		}
	}
}

function checkIsValidSessionData($response){
	if(is_array($response)) printError("корневой объект не должен быть массивом");
	if(!isCorrectStringValue($response -> id_session)) printError("у сессии не установлен id");
	if(!isCorrectStringValue($response -> time_duration)) printError("у сессии не устрановленно время продолжения");
	if(!isCorrectStringValue($response -> time_start)) printError("у сессии не устрановленно время старта");
	if(intval(isCorrectStringValue($response -> id_session)) == 0) printError("не верный формат id сессии");
	if(intval(isCorrectStringValue($response -> time_duration)) == 0) printError("не верный формат время продолжительности сессии");
	if(intval(isCorrectStringValue($response -> time_start)) == 0) printError("не верный формат время старты сессии");
}

function checkIsVariantsJSON($response){
	if(!is_array($response)) printError("корневой объект не массив");
	for($i=0; $i<count($response); $i++){
		$group = $response[$i];
		if(!isCorrectStringValue($group -> id_variant)) printError("у варианта не установлен id");
		if(!isCorrectStringValue($group -> name_variant)) printError("у варианта не устрановленно название");
	}
}

function checkIsGroupsJSON($response){
	if(!is_array($response)) printError("корневой объект не массив");
	for($i=0; $i<count($response); $i++){
		$group = $response[$i];
		if(!isCorrectStringValue($group -> id_group)) printError("у группы не установлен id");
		if(!isCorrectStringValue($group -> name_group)) printError("у групы не устрановленно название");
	}
}

function isCorrectStringValue($value){
	if(isset($value) && ($value != null) && ($value != "")) return true;
	else return false;
}

function checkIsNotErrorMessage($response){
	if(!$response) printError("Возвращен ответ не в формате JSON");
	if(isset($response -> e)) printError("Запрос вернул ошибку с тестом: " . $response -> e);
}

function checkIsCorrectErrorMessage($response){
	if(!$response) printError("Возвращен ответ не в формате JSON");
	if(!isset($response -> e)) printError("Ответ без Е параметра");
	if(!($response -> e) || ($response -> e == "")) printError("Е параметр пуст");
}

function getJSONWithParams($params){
	$query = getRequestWithParams($params);
	$html = getHTML($query);
	checkIsDataEmpty($html);
	checkForMySqlLiks($html);
	$json = json_decode($html);
	checkIsValidJSON($json);
	return $json;
}

function checkForMySqlLiks($html){
	if(isStringContainString($html, "SQL")) printError("раскрытие ошибки MySQL запроса");
}

function checkIsDataEmpty($html){
	if(!isset($html) || !$html || ($html == null) || ($html == "")) printError("пустой или неверный ответ");
}

function checkIsValidJSON($jsonObj){
	if(!isset($jsonObj) || !$jsonObj || ($jsonObj == null)) printError("ошибка парсинга json");
}

function getRequestWithParams($params){
	$keys = array_keys($params);
	$result = APIServer . "?";
	for($i = 0; $i < count($params); $i++){
		if($i == 0) $result .= $keys[$i] ."=".urlencode($params[$keys[$i]]);
		else $result .= "&" . $keys[$i] ."=". urlencode($params[$keys[$i]]);
	}
	return $result;
}



//printing

function printStartMess(){
		echo "<b>".getLogDate()." Test started</b><br/><br/>";
}

function printFinishMess(){
	echo "<br/><b>".getLogDate()." Test finished</b>";
}

function printTestMess($mess){
	printText("<span><b>".getLogDate()." Test: </b></span>" . $mess);
}

function printError($er){
	printText("<b><span style='color:red'>".getLogDate()." Error: </b>" .$er."</span>");
	exit;
}

function printText($text){
	echo $text . "<br/>";
}

function getLogDate(){
	return "[".date("d.m.Y H:i:s")."]";
}

function getHTML($request){
	return file_get_contents($request);
}

function isStringContainString($str, $subStr){
	if (strpos($str, $subStr) !== false) return true;
	return false;
}


?>