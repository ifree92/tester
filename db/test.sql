-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Сен 23 2013 г., 18:03
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `test`
--

-- --------------------------------------------------------

--
-- Структура таблицы `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id_admin` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(20) NOT NULL,
  `passwd` varchar(32) NOT NULL,
  PRIMARY KEY (`id_admin`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `admin`
--

INSERT INTO `admin` (`id_admin`, `login`, `passwd`) VALUES
(1, 'admin', '123');

-- --------------------------------------------------------

--
-- Структура таблицы `answer`
--

CREATE TABLE IF NOT EXISTS `answer` (
  `id_answer` int(11) NOT NULL AUTO_INCREMENT,
  `id_question` int(11) NOT NULL,
  `text` text NOT NULL,
  `is_true` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_answer`),
  KEY `question FK_idx` (`id_question`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Дамп данных таблицы `answer`
--

INSERT INTO `answer` (`id_answer`, `id_question`, `text`, `is_true`) VALUES
(1, 1, 'Ответ 1 на вопрос 1 вариант 1 не правильно', 0),
(2, 1, 'Ответ 2 на вопрос 1 вариант 1 не правильно', 0),
(3, 1, 'Ответ 3 на вопрос 1 вариант 1 не правильно', 0),
(4, 1, 'Ответ 4 на вопрос 1 вариант 1 правильно', 1),
(5, 2, 'Ответ 1 на вопрос 2 вариант 1 не правильно', 0),
(6, 2, 'Ответ 2 на вопрос 2 вариант 1 правильно', 1),
(7, 2, 'Ответ 3 на вопрос 2 вариант 1 не правильно', 0),
(8, 2, 'Ответ 4 на вопрос 2 вариант 1 не правильно', 0),
(9, 3, 'Ответ 1 на вопрос 3 вариант 1 не правильно', 0),
(10, 3, 'Ответ 2 на вопрос 3 вариант 1 не правильно', 0),
(11, 3, 'Ответ 3 на вопрос 3 вариант 1 правильно', 1),
(12, 3, 'Ответ 4 на вопрос 3 вариант 1 не правильно', 0),
(13, 4, 'Ответ 1 на вопрос 1 вариант 2 не правильно', 0),
(14, 4, 'Ответ 2 на вопрос 1 вариант 2 не правильно', 0),
(15, 4, 'Ответ 3 на вопрос 1 вариант 2 не правильно', 0),
(16, 4, 'Ответ 4 на вопрос 1 вариант 2 правильно', 1),
(17, 5, 'Ответ 1 на вопрос 2 вариант 2 не правильно', 0),
(18, 5, 'Ответ 2 на вопрос 2 вариант 2 правильно', 1),
(19, 5, 'Ответ 3 на вопрос 2 вариант 2 не правильно', 0),
(20, 5, 'Ответ 4 на вопрос 2 вариант 2 не правильно', 0),
(21, 6, 'Ответ 1 на вопрос 3 вариант 2 не правильно', 0),
(22, 6, 'Ответ 2 на вопрос 3 вариант 2 не правильно', 0),
(23, 6, 'Ответ 3 на вопрос 3 вариант 2 правильно', 1),
(24, 6, 'Ответ 4 на вопрос 3 вариант 2 не правильно', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `group`
--

CREATE TABLE IF NOT EXISTS `group` (
  `id_group` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id_group`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `group`
--

INSERT INTO `group` (`id_group`, `name`) VALUES
(1, 'КС-13-2м');

-- --------------------------------------------------------

--
-- Структура таблицы `question`
--

CREATE TABLE IF NOT EXISTS `question` (
  `id_question` int(11) NOT NULL AUTO_INCREMENT,
  `id_variant` int(11) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id_question`),
  KEY `variant_idx` (`id_variant`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `question`
--

INSERT INTO `question` (`id_question`, `id_variant`, `text`) VALUES
(1, 1, 'Вопрос 1 вариант 1'),
(2, 1, 'Вопрос 2 вариант 1'),
(3, 1, 'Вопрос 3 вариант 1'),
(4, 2, 'Вопрос 1 вариант 2'),
(5, 2, 'Вопрос 2 вариант 2'),
(6, 2, 'Вопрос 3 вариант 2');

-- --------------------------------------------------------

--
-- Структура таблицы `result`
--

CREATE TABLE IF NOT EXISTS `result` (
  `id_result` int(11) NOT NULL AUTO_INCREMENT,
  `id_answer` int(11) NOT NULL,
  `id_session` int(11) NOT NULL,
  `time` double DEFAULT NULL,
  PRIMARY KEY (`id_result`),
  KEY `rersult_idx` (`id_answer`),
  KEY `result_session FK_idx` (`id_session`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `session`
--

CREATE TABLE IF NOT EXISTS `session` (
  `id_session` int(11) NOT NULL AUTO_INCREMENT,
  `id_group` int(11) NOT NULL,
  `id_variant` int(11) NOT NULL,
  `time` double NOT NULL DEFAULT '0',
  `description` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_session`),
  KEY `session_group FK_idx` (`id_group`),
  KEY `session_variant FK_idx` (`id_variant`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `variant`
--

CREATE TABLE IF NOT EXISTS `variant` (
  `id_variant` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `is_lock` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_variant`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `variant`
--

INSERT INTO `variant` (`id_variant`, `name`, `is_lock`) VALUES
(1, 'Вариант 1', 0),
(2, 'Вариант 2', 0);

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `answer`
--
ALTER TABLE `answer`
  ADD CONSTRAINT `question FK` FOREIGN KEY (`id_question`) REFERENCES `question` (`id_question`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `question`
--
ALTER TABLE `question`
  ADD CONSTRAINT `variant FK` FOREIGN KEY (`id_variant`) REFERENCES `variant` (`id_variant`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `result`
--
ALTER TABLE `result`
  ADD CONSTRAINT `rersult_answer FK` FOREIGN KEY (`id_answer`) REFERENCES `answer` (`id_answer`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `result_session FK` FOREIGN KEY (`id_session`) REFERENCES `session` (`id_session`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `session`
--
ALTER TABLE `session`
  ADD CONSTRAINT `session_group FK` FOREIGN KEY (`id_group`) REFERENCES `group` (`id_group`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `session_variant FK` FOREIGN KEY (`id_variant`) REFERENCES `variant` (`id_variant`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
