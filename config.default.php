<?php

//db_host замените на место размещения БД (обычно localhost)
define("dbHost", "db_host");

//db_user_name замените на имя пользователя БД
define("dbUser", "db_user_name");

//db_password замените на пароль пользователя БД
define("dbPassword", "db_password");

//db_name замените на имя базы данных
define("dbName", "db_name");

//link_to_api замените на ссылку к php файлу API 
//например http://localhost:8888/tester/teste/api.php
define("APIServer", "link_to_api");

?>