<?php
function setLogString($logString, $mode) {

    if ( !is_dir(LOG_DIRECTORY) )  mkdir(LOG_DIRECTORY);

    $fullPath = LOG_DIRECTORY;
    $fullPath.= date("Y-m");
    $fullPath.= "/";

    if (!is_dir($fullPath)) mkdir($fullPath);

    $nameLogFile = date("Y-m-d");
    $nameLogFile.= ".txt";

    // $logFile = fopen("../log/log.txt","a");
    $logFile = fopen($fullPath.$nameLogFile,"a");

    $ip = "[";
    $ip.= date("d/m/Y H:i:s");
    $ip.= "][";
    $ip.= $_SERVER["REMOTE_ADDR"];
    $ip.= "][";

    switch ($mode) {
        case LOG_INFO_MODE: $ip.= "I]"; break;
        case LOG_ERROR_MODE: $ip.= "E]"; break;
        default: $ip.= "~]"; break;
    }

    $ip.= "{";
    $ip.= $logString;
    $ip.= "}\r\n";

    fwrite($logFile, $ip);
    fclose($logFile);
}