<?php

function setJSONInfo($info_str) {
    echo '{"r":"'.$info_str.'"}';
    setLogString($info_str,LOG_INFO_MODE);
}

function setJSONerror($error_str) {
    header("HTTP/1.0 400 setJSONerror");
    echo '{"e":"'.$error_str.'"}';
    setLogString($error_str,LOG_ERROR_MODE);
    exit;
}

function check_response($response, $is_empty=false) {
    if (!$response) {
        //setJSONerror("MySQL error!");
        //header("HTTP/1.0 400 check_response");
        setLogString(mysql_error(),LOG_ERROR_MODE);
        exit;
    } else if ($is_empty == CHECK_EMPTY) {
        if(!mysql_num_rows($response)) {
            //setJSONerror("Empty response");
            header("HTTP/1.0 404");
            exit;
        }
    }
}
