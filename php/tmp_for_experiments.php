<?php

$str = 123;

echo md5($str);

$string = ",10,20";
echo "<br>";
var_dump(explode(",",$string));

echo "<br>";

if (preg_match("(^\d*?(,{0,1}\d+)+)",$string)) {
    echo "true";
} else echo "false";

echo "<br>";

$daa = array(10,20,30,40);

foreach ($daa as &$value) {
    $value = 20;
    echo $value."<br>";
}

var_dump($daa);

echo "<br>";

$arr_id_answers = explode(",","10,20,30,,40,50,");

foreach ($arr_id_answers as $value) {
    if (!filter_var($value,FILTER_VALIDATE_INT)) { echo "error"; exit; }
}