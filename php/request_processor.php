<?php
$request_param = array_merge($_GET, $_POST);
switch($request_param["action"]) {
    case 'tmp': tmp(getParam("tmp")); break;

    case 'get_groups': get_groups(); break;
    case 'unlock_all_variants': checkAccess($request_param["access"]); unlock_all_variants(); break;
    case 'time_duration_get': checkAccess($request_param["access"]); time_duration_get(); break;
    case 'init_db': init_db(); break;

    case 'get_variants': get_variants(getParam("is_lock")); break;
    case 'get_questions': get_questions(getParam("id_session")); break;
    case 'get_result': get_result(getParam("id_session")); break;
    case 'lock_variant': lock_variant(getParam("id_variant")); break;
    case 'unlock_variant': checkAccess($request_param["access"]); unlock_variant(getParam("id_variant")); break;
    case 'new_group': checkAccess($request_param["access"]); new_group(getParam("group_name")); break;
    case 'delete_group': checkAccess($request_param["access"]); delete_group(getParam("id_group")); break;
    case 'delete_variant': checkAccess($request_param["access"]); delete_variant(getParam("id_variant")); break;
    case 'get_questions_by_variant': checkAccess($request_param["access"]); get_questions_by_variant(getParam("id_variant")); break;
    case 'get_answers_by_question': checkAccess($request_param["access"]); get_answers_by_question(getParam("id_question")); break;
    case 'delete_question': checkAccess($request_param["access"]); delete_question(getParam("id_question")); break;
    case 'delete_answer': checkAccess($request_param["access"]); delete_answer(getParam("id_answer")); break;
    case 'finish_session': finish_session(getParam("id_session")); break;
    case 'change_password': checkAccess($request_param["access"]); change_password(getParam("pass")); break;
    case 'time_duration_set': checkAccess($request_param["access"]); time_duration_set(getParam("seconds")); break;

    case 'get_statistic': checkAccess($request_param["access"]); get_statistic(getParam("id_group"),getParam("date"),getParam("id_session")); break;
    case 'add_answer': add_answer(getParam("id_session"),getParam("id_answer")); break;
    case 'log_in': log_in(getParam("login"),getParam("pass")); break;
    case 'update_group': checkAccess($request_param["access"]); update_group(getParam("id_group"),getParam("group_name")); break;
    case 'new_variant': checkAccess($request_param["access"]); new_variant(getParam("variant_name"),getParam("is_lock")); break;
    case 'new_question': checkAccess($request_param["access"]); new_question(getParam("id_variant_FK"),getParam("text")); break;

    case 'update_question': checkAccess($request_param["access"]); update_question(getParam("id_question"),getParam("id_variant"),getParam("text")); break;
    case 'new_answer': checkAccess($request_param["access"]); new_answer(getParam("id_question_FK"),getParam("text"),getParam("is_true")); break;
    case 'start_session': start_session(getParam("name"),getParam("id_group"),getParam("id_variant")); break;
    case 'edit_variant': checkAccess($request_param["access"]); edit_variant(getParam("id_variant"),getParam("variant_name"),getParam("is_lock")); break;

    case 'update_answer': checkAccess($request_param["access"]); update_answer(getParam("id_answer"),getParam("id_question_FK"),getParam("text"),getParam("is_true")); break;

    case 'get_db_var_dump': checkAccess(); get_db_var_dump(); break;
    case 'set_db_var_dump': checkAccess(); try_set_db_var_dump(); break;
    case 'clear_db_var_dump': checkAccess(); clear_db_var_dump(); break;

    default: echo "Hello! This is Tester API.</br>";
    echo '<a href="api.php?action=get_groups">Показать все группы</a></br>';
    echo '<a href="api.php?action=get_variants">Показать все варианты</a></br>';
    echo '<a href="api.php?action=start_session&name=thisisaname&id_group=1&id_variant=1">Старт сессии</a></br>';
    echo '<a href="api.php?action=get_questions&id_session=1">Показать все вопросы</a></br>';
    echo '<a href="api.php?action=add_answer&id_session=1&id_answer=11">Добавление текущего ответа в БД</a></br>';
    echo '<a href="api.php?action=get_result&id_session=1">Получить правильные ответы</a></br>';
    echo '<a href="api.php?action=get_statistic">Получить всю статистику</a></br>';
    echo '<a href="api.php?action=get_statistic&id_group=1">Получить всю статистику по 1й группе</a></br>';
    echo '<a href="api.php?action=get_questions_by_variant&id_variant=1">Получить все вопросы по заданному варианту</a></br>';
    echo '<a href="api.php?action=get_answers_by_question&id_question=1">Получить все варианты ответов по заданному вопросу</a></br>';
}

function try_set_db_var_dump() {
    if (set_db_var_dump()) {
        echo "База данных загружена";
    } else {
        echo "Ошибка загрузки данных";
    }
    echo "<br><a href='/admin.html'>Вернуться</a>";
}

function getParam($paramName) {     //получить параметр по имени
    global $request_param;
    if ( isset($request_param[$paramName]) ) {
        return $request_param[$paramName];
    }
    return null;
}

function checkAccess($ucode=null) {
    $ucode = "";
    $ucode = $_COOKIE['access'];

    if (($ucode!=null) && ($scode = file_get_contents("access.cfg")) ) {
        if ($scode === $ucode) return true;
    }
    header("HTTP/1.0 401");
    echo "<h1>401 Authorization Error</h1>";
    exit;
}