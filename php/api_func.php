<?php
function get_groups() {
    $query = "SELECT `id_group`,`name` FROM `group`";
    $response = mysql_query($query);
    check_response($response, CHECK_EMPTY);
    $json_array = array();
    while ($row = mysql_fetch_assoc($response)) {
        array_push($json_array, array("id_group" => $row["id_group"], "name_group" => $row["name"]));
    }
    echo get_json_cyr_fix_str(json_encode($json_array));
}

function get_variants($is_lock=null) {
    $query = "";
    $options = array("options" => array("min_range" => 0, "max_range" => 1, "default" => -1));

    if ($is_lock!=null) {
        if (filter_var($is_lock,FILTER_VALIDATE_INT,$options) == -1) setJSONerror("is_lock is not valid and can be only 0 or 1");
        $query .= "SELECT `id_variant`,`name`,`is_lock` FROM `variant` WHERE `is_lock`=".$is_lock;
    } else {
        $query .= "SELECT `id_variant`, `name`,`is_lock` FROM `variant`";
    }

    $response = mysql_query($query);
    check_response($response,CHECK_EMPTY);
    $json_array = array();
    while ($row = mysql_fetch_assoc($response)) {
        array_push($json_array, array("id_variant" => $row["id_variant"], "name_variant" => $row["name"], "is_lock" => $row["is_lock"]));
    }
    echo get_json_cyr_fix_str(json_encode($json_array));
}

function start_session($name, $id_group, $id_variant) {
    $time_duration = time_duration_get(true);
    $time_start = time();

    if (empty($name)) setJSONerror("name is not valid");
    if (!filter_var($id_group,FILTER_VALIDATE_INT)) setJSONerror("id_group is not valid");
    if (!filter_var($id_variant,FILTER_VALIDATE_INT)) setJSONerror("id_variant is not valid");

    $query_insert_session = "INSERT INTO `session` (`id_group`, `id_variant`, `time`, `description`) VALUES ('".$id_group."', '".$id_variant."', '".$time_start."', '".addslashes($name)."')";

    $response_insert = mysql_query($query_insert_session);
    check_response($response_insert,DONT_CHECK_EMPTY);
    if ( !($id_session = mysql_insert_id())) { setJSONerror("Cannot request id_session"); exit; }

    $json_array = array("id_session" => $id_session, "time_duration" => ''.$time_duration.'', "time_start" => ''.$time_start.'');

    echo json_encode($json_array);
}

function get_questions($id_session) {
    if(!filter_var($id_session,FILTER_VALIDATE_INT)) setJSONerror("id_session is not valid");
    $variant_response = mysql_query("SELECT `id_variant` FROM `session` WHERE `id_session`=".$id_session);
    if ($variant_response && (mysql_num_rows($variant_response)==1) ) {
        $variant = mysql_result($variant_response,0);
    } else {
        setJSONerror("no variant for this id_session");
        exit;
    }
    $response = mysql_query("SELECT `id_question`,`text` FROM `question` WHERE `id_variant` = ".$variant);
    check_response($response,DONT_CHECK_EMPTY);
    $json_array = array();
    while ($row = mysql_fetch_assoc($response)) {
        $answ_response = mysql_query("SELECT `id_answer`,`text`,`is_true` FROM `answer` WHERE `id_question` = ".$row["id_question"]);
        check_response($answ_response,DONT_CHECK_EMPTY);
        $answ_array = array();
        $counter_is_true = 0;
        while ($answ_row = mysql_fetch_assoc($answ_response)) {
            array_push($answ_array,array("id_answer" => $answ_row["id_answer"],"text_answer" => $answ_row["text"]));
            if ($answ_row["is_true"] == "1") $counter_is_true++;
        }
        array_push($json_array, array("id_question" => $row["id_question"],"text_question" => $row["text"],"count_rights" => $counter_is_true,"answer" => $answ_array));
    }
    echo get_json_cyr_fix_str(json_encode($json_array));
}

function add_answer($id_session, $id_answer) {

    if (!filter_var($id_session,FILTER_VALIDATE_INT)) setJSONerror("id_session is not valid");
//    if (!filter_var($id_answer,FILTER_VALIDATE_INT)) setJSONerror("id_answer is not valid");
    if (empty($id_answer)) setJSONerror("id_answer is empty");

    $arr_id_answers = explode(",",$id_answer);

    foreach ($arr_id_answers as $value) {
        if (!filter_var($value,FILTER_VALIDATE_INT)) setJSONerror("id_answer is not valid");
    }

    $cur_time = time();
    $query = "INSERT INTO `result` (`id_answer`, `id_session`, `time`) VALUES";
    foreach ($arr_id_answers as $value) {
        $query .= "('".$value."', '".$id_session."', '".$cur_time."'),";
    }
    $query = substr($query,0,strlen($query)-1);
    check_response(mysql_query($query),DONT_CHECK_EMPTY);
}

function get_result($id_session) {

    if(!filter_var($id_session,FILTER_VALIDATE_INT)) setJSONerror("id_session is not valid");
    $where = "WHERE `id_session`=".$id_session;

    $query_questions = "SELECT `q`.`id_question`,`s`.`id_variant`,`q`.`text`,
                            (SELECT COUNT(*)
	                        FROM `answer` as `a`
	                        WHERE `a`.`id_question` = `q`.`id_question`
		                        AND
		                        `a`.`is_true` = 1
                            ) as `count_rights`
                        FROM `session` as `s`
	                        RIGHT JOIN `question` as `q`
	                        ON `q`.`id_variant` = `s`.`id_variant`".$where;

    $query_results = "SELECT `a`.`id_question`,`a`.`id_answer`,`a`.`is_true`,`r`.`time`,`a`.`text`
                      FROM `result` as `r`
	                    LEFT JOIN `answer` as `a`
	                    ON `a`.`id_answer` = `r`.`id_answer`".$where;

    $response_questions = mysql_query($query_questions);
    $response_results = mysql_query($query_results);
    check_response($response_questions,DONT_CHECK_EMPTY);
    check_response($response_results,DONT_CHECK_EMPTY);
    $json_array_questions = array();
    $json_array_results = array();
    while ($row = mysql_fetch_assoc($response_questions)) {
        array_push($json_array_questions,array("id_question" => $row["id_question"], "text" => $row["text"], "count_rights" => $row["count_rights"]));
    }
    while ($row = mysql_fetch_assoc($response_results)) {
        array_push($json_array_results,array("id_question" => $row["id_question"],"id_answer" => $row["id_answer"],
            "is_true" => $row["is_true"],"time" => $row["time"],"text" => $row["text"]));
    }
    foreach ($json_array_questions as $key_q => $value_q) {
        $tmp_answers = array();
        $count_true_answer = 0;
        $count_false_answer = 0;
        foreach ($json_array_results as $key_r => $value_r)
            if ($value_q["id_question"] === $value_r["id_question"]) {
                array_push($tmp_answers,$value_r);
                $value_r["is_true"] === "1" ? $count_true_answer++ : $count_false_answer++;
            }
        if ($count_false_answer == 0 && $count_true_answer == $json_array_questions[$key_q]["count_rights"] && $count_true_answer!=0) {
            $json_array_questions[$key_q]["is_right_question"] = "1";
        } else
            $json_array_questions[$key_q]["is_right_question"] = "0";
        $json_array_questions[$key_q]["answers"] = $tmp_answers;

    }
    echo get_json_cyr_fix_str(json_encode($json_array_questions));
}

function finish_session($id_session) {
    if(!filter_var($id_session,FILTER_VALIDATE_INT)) setJSONerror("id_session is not valid");
    $query_finish_session = "UPDATE `session` SET `is_finish`='1',`time_finish`=".time()." WHERE `id_session`=".$id_session;
    check_response(mysql_query($query_finish_session),DONT_CHECK_EMPTY);
    echo get_statistic(null,null,$id_session,true);
}

function lock_variant($id_variant) {

    if (!filter_var($id_variant,FILTER_VALIDATE_INT)) setJSONerror("id_variant is not valid");

    $request_select = "SELECT `is_lock` FROM `variant` WHERE `id_variant` = ".$id_variant;
    $request_update = "UPDATE `variant` SET `is_lock`='1' WHERE `id_variant`= ".$id_variant;

    $response_is_lock = mysql_query($request_select);

    check_response($response_is_lock, CHECK_EMPTY);

    $row = mysql_fetch_assoc($response_is_lock);
    if ($row["is_lock"]) echo '{"success":"0"}';
    else {
        check_response(mysql_query($request_update),DONT_CHECK_EMPTY);
        echo '{"success":"1"}';
    }
}

function unlock_variant($id_variant) {

    if (!filter_var($id_variant,FILTER_VALIDATE_INT)) setJSONerror("id_variant is not valid");

    $request_select = "SELECT `is_lock` FROM `variant` WHERE `id_variant` = ".$id_variant;
    $request_update = "UPDATE `variant` SET `is_lock`='0' WHERE `id_variant`= ".$id_variant;

    $response_is_lock = mysql_query($request_select);

    check_response($response_is_lock, CHECK_EMPTY);

    $row = mysql_fetch_assoc($response_is_lock);
    if ($row["is_lock"] == 0) setJSONInfo("variant already unlocked");
    else {
        check_response(mysql_query($request_update),DONT_CHECK_EMPTY);
        setJSONInfo("Success");
    }
}

function unlock_all_variants() {
    $query = "UPDATE `variant` SET `is_lock`=0 WHERE `is_lock`=1";
    check_response(mysql_query($query),DONT_CHECK_EMPTY);
}

function generateCode($length=6) {
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRQSTUVWXYZ0123456789";
    $code = "";
    $clen = strlen($chars) - 1;
    while (strlen($code) < $length) {
        $code .= $chars[mt_rand(0,$clen)];
    }
    return $code;
}

function log_in($login, $pass) {
    if(empty($login) || empty($pass)) setJSONerror("Empty variable!");

    $_login = trim($login);
    $_pass = md5(trim($pass));

    if (strpos($_login, 0x20) || strpos($_pass, 0x20) ) setJSONerror("Syntax error!");
    $query = "SELECT id_admin FROM admin where login = '".$_login."' && passwd = '".$_pass."'";
    $response = mysql_query($query);

    check_response($response,CHECK_EMPTY);

    $row = mysql_fetch_array($response);
    $code = generateCode();
    if (file_put_contents("access.cfg",$code)) {
        echo $code;
        return;
    }
    header("HTTP/1.0 400 login_crash");
}

function change_password($pass) {
    if(empty($pass)) setJSONerror("Empty variable!");
    $query = "UPDATE `admin` SET `passwd`='".md5($pass)."' WHERE `id_admin`='1'";
    check_response(mysql_query($query));
    file_put_contents("access.cfg","");
    echo "";
}

function get_statistic($id_group,$date,$id_session,$special_access_for_get_result=null) {
    $query = "SELECT `s`.`id_session`,`g`.`name` as `group_name`,`v`.`name` as `variant_name`,`s`.`time`,`description`,`s`.`time_finish`,`s`.`is_finish`,
(SELECT COUNT(*) FROM `question` WHERE `id_variant` = `s`.`id_variant`) as `count_questions`,
(SELECT COUNT(*)
	FROM `question` as `q2`
	WHERE `q2`.`id_variant`=`s`.`id_variant`
	AND(SELECT COUNT(*)
			FROM `answer` as `a`
			WHERE `a`.`id_question`=`q2`.`id_question`
			AND `a`.`is_true`=1
	)=(SELECT COUNT(*)
			FROM `result` as `r`
			INNER JOIN `answer` as `a2`
			ON `r`.`id_answer`=`a2`.`id_answer`
			WHERE `a2`.`id_question`=`q2`.`id_question`
			AND `a2`.`is_true`=1
			AND `r`.`id_session`=`s`.`id_session`)
	AND(SELECT COUNT(*)
			FROM `result` as `r`
			INNER JOIN `answer` as `a2`
			ON `r`.`id_answer`=`a2`.`id_answer`
			WHERE `a2`.`id_question`=`q2`.`id_question`
			AND `a2`.`is_true`=0
			AND `r`.`id_session`=`s`.`id_session`)=0
) as `right_questions`

FROM `session` as `s`
INNER JOIN `variant` as `v`
ON `v`.`id_variant` = `s`.`id_variant`
INNER JOIN `group` as `g`
ON `g`.`id_group` = `s`.`id_group`";

    $first_concat = true;   //если первый раз то WHERE иначе AND
    if (isset($id_group)) { //фильтр по группе
        if(!filter_var($id_group,FILTER_VALIDATE_INT)) setJSONerror("id_group is not valid");
        $query .= "WHERE `s`.`id_group`=".$id_group;
        $first_concat = false;
    }

    if (isset($date)) {     //фильтр по дате
        if(!filter_var($date,FILTER_VALIDATE_INT)) setJSONerror("date is not valid");
        $query.=($first_concat)?"WHERE ":" AND ";
        $query.="DATE(FROM_UNIXTIME(`s`.`time`))=DATE(FROM_UNIXTIME(".$date."))";
        $first_concat = false;
    }

    if (isset($id_session)) {   //фильтр по сессии
        if(!filter_var($id_session,FILTER_VALIDATE_INT)) setJSONerror("id_session is not valid");
        $query.=($first_concat)? "WHERE" : "AND";
        $query.="`s`.`id_session`=".$id_session;
        $first_concat = false;
    }

    if ($first_concat) {
        $query.=" LIMIT 50";
    }

    $response_result = mysql_query($query);
    check_response($response_result,DONT_CHECK_EMPTY);
    $json_array = array();
    while ($row = mysql_fetch_array($response_result)) {
        array_push($json_array,array("id_session" => $row["id_session"],"group_name" => $row["group_name"],
            "variant_name" => $row["variant_name"],"time" => $row["time"],"description" => $row["description"],
            "time_finish" => $row["time_finish"],"is_finish" => $row["is_finish"],"count_questions" => $row["count_questions"],
            "right_questions" => $row["right_questions"]));
    }
    if($special_access_for_get_result) {
        return get_json_cyr_fix_str(json_encode($json_array));
    } else {
        echo get_json_cyr_fix_str(json_encode($json_array));
    }
}

function new_group($group_name) {
    if(empty($group_name)) setJSONerror("Empty variable!");

    $id_inserted_group = null;

    $query = "INSERT INTO `group` (`name`) VALUES ('".addslashes($group_name)."')";
    check_response(mysql_query($query),DONT_CHECK_EMPTY);

    if (!($id_inserted_group = mysql_insert_id())) { setJSONerror("Cannot insert new group"); exit; }

    echo '{"id_group":"'.$id_inserted_group.'"}';
}

function update_group($id_group, $group_name) {
    if (empty($group_name)) setJSONerror("Empty variable");
    if(!filter_var($id_group,FILTER_VALIDATE_INT)) setJSONerror("id_group is not valid");
    $query_update = "UPDATE `group` SET `name`='".addslashes($group_name)."' WHERE `id_group`=".$id_group;
    check_response(mysql_query($query_update),DONT_CHECK_EMPTY);
}

function delete_group($id_group) {
    if(!filter_var($id_group,FILTER_VALIDATE_INT)) setJSONerror("id_group is not valid");
    $query_delete = "DELETE FROM `group` WHERE `id_group`=".$id_group;
    check_response(mysql_query($query_delete),DONT_CHECK_EMPTY);
}

function edit_variant($id_variant,$name_variant,$is_lock) {
    $options = array("options" => array("min_range" => 0, "max_range" => 1, "default" => -1));
    if(!filter_var($id_variant,FILTER_VALIDATE_INT)) setJSONerror("id_variant is not valid");
    if(filter_var($is_lock,FILTER_VALIDATE_INT,$options) == -1) setJSONerror("is_lock is not valid and can be only 0 or 1");
    if(empty($name_variant)) setJSONerror("Empty name_variant");
    $query = "UPDATE `variant` SET `name`='".addslashes($name_variant)."', `is_lock`=".$is_lock." WHERE `id_variant`=".$id_variant;
    check_response(mysql_query($query),DONT_CHECK_EMPTY);
}

function new_variant($name_variant,$is_lock) {
    $options = array("options" => array("min_range" => 0, "max_range" => 1, "default" => -1));
    if(filter_var($is_lock,FILTER_VALIDATE_INT,$options) == -1) setJSONerror("is_lock is not valid and can be only 0 or 1");
    if(empty($name_variant)) setJSONerror("Empty name_variant");
    $query = "INSERT INTO `variant` (`name`, `is_lock`) VALUES ('".addslashes($name_variant)."', ".$is_lock.")";
    check_response(mysql_query($query),DONT_CHECK_EMPTY);
    if (!($id = mysql_insert_id())) { setJSONerror("Cannot insert new variant"); exit; }
    echo '{"id_variant":"'.$id.'"}';
}

function delete_variant($id_variant) {
    if(!filter_var($id_variant,FILTER_VALIDATE_INT)) setJSONerror("id_variant is not valid");
    $query = "DELETE FROM `variant` WHERE `id_variant`=".$id_variant;
    check_response(mysql_query($query),DONT_CHECK_EMPTY);
}

function new_question($id_variant_FK,$text) {
    if(!filter_var($id_variant_FK,FILTER_VALIDATE_INT)) setJSONerror("id_variant_FK is not valid");
    if(empty($text)) setJSONerror("Empty text");

    $query = "INSERT INTO `question` (`id_variant`, `text`) VALUES (".$id_variant_FK.", '".addslashes($text)."')";
    check_response(mysql_query($query),DONT_CHECK_EMPTY);
    if (!($id = mysql_insert_id())) { setJSONerror("Cannot insert new question"); exit; }
    echo '{"id_question":"'.$id.'"}';
}

function new_answer($id_question_FK,$text,$is_true) {
    $options = array("options" => array("min_range" => 0, "max_range" => 1, "default" => -1));
    if(empty($text)) setJSONerror("Empty text");
    if(!filter_var($id_question_FK,FILTER_VALIDATE_INT)) setJSONerror("id_question_FK is not valid");
    if(filter_var($is_true,FILTER_VALIDATE_INT,$options) == -1) setJSONerror("is_true is not valid and can be only 0 or 1");

    $query = "INSERT INTO `answer` (`id_question`, `text`, `is_true`) VALUES (".$id_question_FK.", '".addslashes($text)."', ".$is_true.")";
    check_response(mysql_query($query),DONT_CHECK_EMPTY);
    if (!($id = mysql_insert_id())) { setJSONerror("Cannot insert new answer"); exit; }
    echo '{"id_answer":"'.$id.'"}';
}

function get_questions_by_variant($id_variant) {
    if(!filter_var($id_variant,FILTER_VALIDATE_INT)) setJSONerror("id_variant is not valid");
    $query = "SELECT `id_question`,`text` FROM question WHERE id_variant=".$id_variant;
    $response = mysql_query($query);
    check_response($response,DONT_CHECK_EMPTY);
    $json_array = array();
    while ($row = mysql_fetch_assoc($response)) {
        array_push($json_array,array("id_question" => $row["id_question"],"text" => $row["text"]));
    }
    echo get_json_cyr_fix_str(json_encode($json_array));
}

function get_answers_by_question($id_question) {
    if(!filter_var($id_question,FILTER_VALIDATE_INT)) setJSONerror("id_question is not valid");
    $query = "SELECT `id_answer`,`text`,`is_true` FROM `answer` WHERE `id_question`=".$id_question;
    $response = mysql_query($query);
    check_response($response,DONT_CHECK_EMPTY);
    $json_array = array();
    while ($row = mysql_fetch_assoc($response)) {
        array_push($json_array,array("id_answer" => $row["id_answer"],"text" => $row["text"],"is_true" => $row["is_true"]));
    }
    echo get_json_cyr_fix_str(json_encode($json_array));
}

function update_question($id_question,$id_variant,$text) {
    if(!filter_var($id_question,FILTER_VALIDATE_INT)) setJSONerror("id_question is not valid");
    if(empty($text)) setJSONerror("text is not valid");
    if(filter_var($id_variant,FILTER_VALIDATE_INT)) {
        $query = "UPDATE `question` SET `id_variant`=".$id_variant.", `text`='".addslashes($text)."' WHERE `id_question`=".$id_question;
        check_response(mysql_query($query),DONT_CHECK_EMPTY);
    } else if (empty($id_variant)) {
        $query = "UPDATE `question` SET `text`='".addslashes($text)."' WHERE `id_question`=".$id_question;
        check_response(mysql_query($query),DONT_CHECK_EMPTY);
    } else {
        setJSONerror("id_question is not valid");
    }
}

function delete_question($id_question) {
    if(!filter_var($id_question,FILTER_VALIDATE_INT)) setJSONerror("id_question is not valid");
    $query = "DELETE FROM `question` WHERE `id_question`=".$id_question;
    check_response(mysql_query($query),DONT_CHECK_EMPTY);
}

function update_answer($id_answer,$id_question_FK,$text,$is_true) {
    if(!filter_var($id_answer,FILTER_VALIDATE_INT)) setJSONerror("id_answer is not valid");
    $selection = 0;
    if(!empty($id_question_FK)) {
        if(filter_var($id_question_FK,FILTER_VALIDATE_INT)) $selection += 1;
        else setJSONerror("id_question is not valid");
    }

    if(!empty($text)) {
        $selection += 2;
    }

    if($is_true!=null) {
        $options = array("options" => array("min_range" => 0, "max_range" => 1, "default" => -1));
        if(filter_var($is_true,FILTER_VALIDATE_INT,$options)!= -1) $selection += 4;
        else setJSONerror("is_true is not valid and can be only 0 or 1");
    }

    $query = "UPDATE `answer` SET";

    switch($selection) {
        case '1':
            $query .= "`id_question`=".$id_question_FK;
            break;
        case '2':
            $query .= "`text`=".addslashes($text);
            break;
        case '4':
            $query .= "`is_true`=".$is_true;
            break;
        case '3':
            $query .= "`id_question`=".$id_question_FK.", `text`='".addslashes($text)."'";
            break;
        case '5':
            $query .= "`id_question`=".$id_question_FK.", `is_true`=".$is_true;
            break;
        case '6':
            $query .= "`text`='".addslashes($text)."', `is_true`=".$is_true;
            break;
        case '7':
            $query .= "`id_question`=".$id_question_FK.", `text`='".addslashes($text)."', `is_true`=".$is_true;
            break;
        default: setJSONerror("Some error in update_answer method");
    }

    $query .= " WHERE `id_answer`=".$id_answer;
    echo $query;
    check_response(mysql_query($query),DONT_CHECK_EMPTY);
}

function delete_answer($id_answer) {
    if(!filter_var($id_answer,FILTER_VALIDATE_INT)) setJSONerror("id_answer is not valid");
    $query = "DELETE FROM `answer` WHERE `id_answer`=".$id_answer;
    check_response(mysql_query($query),DONT_CHECK_EMPTY);
}

function set_db_var_dump() {
    global $mysqli_session;
    if (isset($_FILES['FILE']['name'])) {
        $file_name = $_FILES['FILE']['name'];
        $filetype = substr($file_name, strlen($file_name) - 4);
        if ($filetype == "json") {
            if ($_FILES['FILE']['size'] != 0 AND $_FILES['FILE']['size']<=31457280) {
                if (is_uploaded_file($_FILES['FILE']['tmp_name'])) {
                    $file_data = file_get_contents($_FILES['FILE']['tmp_name']);
                    if (!$file_data) return false;
                    $data = json_decode($file_data);
                    if (is_array($data)) {
                        $query = "START TRANSACTION;";
                        for ($i=0;$i<count($data);++$i) {
                            $query.="INSERT INTO `variant` (`name`) VALUES ('".addslashes($data[$i]->name)."');SELECT LAST_INSERT_ID() INTO @varid;";
                            for ($j=0;$j<count($data[$i]->question);++$j) {
                                $query.="INSERT INTO`question` (`id_variant`,`text`) VALUES (@varid,'".addslashes($data[$i]->question[$j]->text)."');SELECT LAST_INSERT_ID() INTO @questid;";
                                for ($k=0;$k<count($data[$i]->question[$j]->answer);++$k) {
                                    $query.="INSERT INTO `answer` (`id_question`,`text`,`is_true`) VALUES (@questid,'".addslashes($data[$i]->question[$j]->answer[$k]->text)."','".addslashes($data[$i]->question[$j]->answer[$k]->true)."');";
                                }
                            }
                        }
                        $query.="COMMIT;";
                        return $mysqli_session->multi_query($query);
                    }
                    return false;
                }
            }
        }
    }
    return false;
}

function get_db_var_dump() {
    $variant = array();
    $query_var = "SELECT `id_variant`,`name` FROM `variant`;";
    $result_var = mysql_query($query_var);
    while ($row_var = mysql_fetch_assoc($result_var)) {
        $question = array();
        $query_quest = "SELECT `id_question`,`text` FROM `question` WHERE `id_variant`=".$row_var["id_variant"].";";
        $result_quest = mysql_query($query_quest);
        while ($row_quest = mysql_fetch_assoc($result_quest)) {
            $answer = array();
            $query_answer = "SELECT `text`,`is_true` FROM `answer` WHERE `id_question`=".$row_quest["id_question"].";";
            $result_answer = mysql_query($query_answer);
            while ($row_answer = mysql_fetch_assoc($result_answer)) {
                array_push($answer,array("text"=>$row_answer["text"],"true"=>$row_answer["is_true"]));
            }
            array_push($question,array("text"=>$row_quest["text"],"answer"=>$answer));
        };
        array_push($variant,array("name"=>$row_var["name"],"question"=>$question));
    };
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename=test_'.date('Y-m-d_H-i-s').".json");
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    echo get_json_cyr_fix_str(json_encode($variant));
}

function clear_db_var_dump() {
    global $mysqli_session;
    if ($mysqli_session->query("DELETE FROM `variant`")) {
        exit;
    } else {
        setJSONerror("clear error");
    }
}

function time_duration_set($seconds) {
    if(!filter_var($seconds,FILTER_VALIDATE_INT)) setJSONerror("seconds is not valid");
    $query = "UPDATE `settings` SET `value`='".$seconds."' WHERE `name`='time_duration'";
    check_response(mysql_query($query),DONT_CHECK_EMPTY);
}

function time_duration_get($is_return_only=null) {
    $query = "SELECT `value` FROM `settings` WHERE `name`='time_duration'";
    $response = mysql_query($query);
    check_response($response,CHECK_EMPTY);
    $row = mysql_fetch_assoc($response);
    if ($is_return_only)
        return $row["value"];
    else
        echo $row["value"];
}

function init_db() {
    global $mysqli_session;
    $res =  $mysqli_session->query("show tables");
    if (mysqli_num_rows($res)==0) {
        $query = "CREATE TABLE IF NOT EXISTS `admin` (
  `id_admin` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(20) NOT NULL,
  `passwd` varchar(32) NOT NULL,
  `access` varchar(32) DEFAULT '',
  PRIMARY KEY (`id_admin`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

INSERT INTO `admin` (`id_admin`, `login`, `passwd`) VALUES (1, 'admin', '202cb962ac59075b964b07152d234b70');

CREATE TABLE IF NOT EXISTS `answer` (
  `id_answer` int(11) NOT NULL AUTO_INCREMENT,
  `id_question` int(11) NOT NULL,
  `text` text NOT NULL,
  `is_true` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_answer`),
  KEY `question FK_idx` (`id_question`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `group` (
  `id_group` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id_group`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `question` (
  `id_question` int(11) NOT NULL AUTO_INCREMENT,
  `id_variant` int(11) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id_question`),
  KEY `variant_idx` (`id_variant`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `result` (
  `id_result` int(11) NOT NULL AUTO_INCREMENT,
  `id_answer` int(11) NOT NULL,
  `id_session` int(11) NOT NULL,
  `time` double DEFAULT NULL,
  PRIMARY KEY (`id_result`),
  KEY `rersult_idx` (`id_answer`),
  KEY `result_session FK_idx` (`id_session`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `session` (
  `id_session` int(11) NOT NULL AUTO_INCREMENT,
  `id_group` int(11) NOT NULL,
  `id_variant` int(11) NOT NULL,
  `time` double NOT NULL DEFAULT '0',
  `description` varchar(20) DEFAULT NULL,
  `time_finish` double NOT NULL DEFAULT '0',
  `is_finish` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_session`),
  KEY `session_group FK_idx` (`id_group`),
  KEY `session_variant FK_idx` (`id_variant`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `variant` (
  `id_variant` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `is_lock` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_variant`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `settings` (
  `name` varchar(200) NOT NULL,
  `value` int(11) NOT NULL,
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `settings` (`name`, `value`) VALUES
('time_duration', 600);

ALTER TABLE `answer`
ADD CONSTRAINT `question FK` FOREIGN KEY (`id_question`) REFERENCES `question` (`id_question`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `question`
ADD CONSTRAINT `variant FK` FOREIGN KEY (`id_variant`) REFERENCES `variant` (`id_variant`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `result`
ADD CONSTRAINT `result_session FK` FOREIGN KEY (`id_session`) REFERENCES `session` (`id_session`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `rersult_answer FK` FOREIGN KEY (`id_answer`) REFERENCES `answer` (`id_answer`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `session`
ADD CONSTRAINT `session_group FK` FOREIGN KEY (`id_group`) REFERENCES `group` (`id_group`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `session_variant FK` FOREIGN KEY (`id_variant`) REFERENCES `variant` (`id_variant`) ON DELETE CASCADE ON UPDATE CASCADE;";
        $res = $mysqli_session->multi_query($query);
        if ($mysqli_session->error=="") {
            echo "DB was initialized";
            exit;
        }
        echo $mysqli_session->error;
    }
    echo "DB allredy up";
}

function tmp($tmp) {
    $string = "10,44,21,999";
    echo preg_match_all("[0-9]*[0-9]",$string,$out);
    echo $out;
}